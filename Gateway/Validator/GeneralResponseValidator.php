<?php


namespace Pronko\Authorizenet\Gateway\Validator;


use Magento\Payment\Gateway\Validator\AbstractValidator;

class GeneralResponseValidator extends AbstractValidator
{
    public function validate(array $validationSubject)
    {
        $response = $validationSubject['response'];
        $isValid = true;
        $errorMessages = [];

        foreach ($this->getResponseValidator($response) as $validator) {

            $responseValidator = $validator($response);
            if (!$responseValidator[0]) {
                $isValid = $responseValidator[0];
                $errorMessages = array_merge($errorMessages, $responseValidator[1]);
            }
        }
        return $this->createResult($isValid, $errorMessages);
    }

    private function getResponseValidator($response)
    {
        return [
            function ($response) {
                return [
                    isset($response['transactionResponse']) && is_array($response['transactionResponse']),
                    [__('Authorizenet response errror')]
                ];
            },
            function ($response) {
                return [
                    isset($response['messages']['resultCode']) && ('Ok' === $response['messages']['resultCode']),
                    [$response['messages']['message'][0]['text'] ?: __('Authorizenet response errror')]
                ];
            },
            function ($response) {
                return [
                    empty($response['transactionResponse']['errors']),
                    [isset($response['transactionResponse']['errors'][0]['errorText']) ?
                        $response['transactionResponse']['errors'][0]['errorText'] :
                        __('Authorizenet response errror')]
                ];
            }
        ];
    }


}