<?php namespace Pronko\Authorizenet\Gateway\Request\Builder;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;

class ProductItems implements BuilderInterface
{
    /**
     * 
     * @param array $buildSubject
     * @return type
     */

    public function build(array $buildSubject)
    {
        /** @var PaymentDataObjectInterface $paymentDataObject  * */
        $paymentDataObject = $buildSubject['payment'];
        $order = $paymentDataObject->getOrder();
        $items = [];
        /** @var OrderItemInterface $item  */
        foreach ($order->getItems() as $key => $item) {
            $items['lineItem'][] = [
                'itemId' => (string) $key,
                'name' => substr($item->getName(), 0, 31),
                'description' => substr($item->getDescription(), 0, 255),
                'quantity' => (string) $item->getQtyOrdered(),
                'unitPrice' => $item->getPrice()
            ];
        }
        
        return [
            'lineItems' => $items
        ];

        /*
          <lineItems>
          <lineItem>
          <itemId>1</itemId>
          <name>vase</name>
          <description>Cannes logo </description>
          <quantity>18</quantity>
          <unitPrice>45.00</unitPrice>
          </lineItem> */
    }
}
