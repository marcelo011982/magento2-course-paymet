<?php

namespace Pronko\Authorizenet\Gateway;


use Magento\Framework\Exception\NotFoundException;
use Magento\Payment\Gateway\Config\ValueHandlerPool;

class Config
{
    /**
     * @var ValueHandlerPool
     */
    private $valueHandlerPool;

    /**
     * Config constructor.
     * @param ValueHandlerPoolInterface $valueHandlerPoolInterface
     */
    public function __construct(ValueHandlerPool $valueHandlePool)
    {
        $this->valueHandlerPool = $valueHandlePool;
    }


    public function getGatewayUrl()
    {
        if ($this->isSandBox()) {
            return (string)$this->getValue('gateway_url_sandbox');
        }
        return (string)$this->getValue('gateway_url');
    }

    /**
     * @return bool
     */
    public function isSandBox()
    {
        return (bool)$this->getValue('is_sandbox');

    }

    /**
     * @return array
     */
    public function getGatewayHeaders()
    {
        return ['Content-Type' => 'application/json'];

    }

    /**
     * @return string
     */
    public function getApiLoginId()
    {
        return (string) $this->getValue('api_login_id');
    }

    /**
     * @return string
     */
    public function getTransactionKey()
    {
        return (string) $this->getValue('transaction_key');
    }


    private function getValue($field)
    {
        try {
            $handler = $this->valueHandlerPool->get($field);
            return $handler->handle(['field' => $field]);
        } catch (NotFoundException $exception) {
            return null;
        }

    }
}
