<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Pronko\Authorizenet\Gateway\Http;

use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Payment\Gateway\Http\ConverterInterface;
use Magento\Payment\Model\Method\Logger;
use Magento\Payment\Gateway\Http\ConverterException;
use Magento\Payment\Gateway\Http\ClientException;

/**
 * Description of Client
 *
 * @author marcelo
 */
class Client implements ClientInterface
{

    /**
     *
     * @var ZendClientFactory
     */
    private $clientFactory;

    /**
     *
     * @var ConverterInterface 
     */
    private $converter;

    /**
     *
     * @var Logger
     */
    private $logger;

    public function __construct(
        ZendClientFactory $clientFactory,
        Logger $logger,
        ConverterInterface $converter = null
    )
    {
        $this->clientFactory = $clientFactory;
        $this->logger = $logger;
        $this->converter = $converter;
    }

    /**
     * 
     * @param TransferInterface $transferObject
     * @return array
     */
    public function placeRequest(TransferInterface $transferObject)
    {
        \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Psr\Log\LoggerInterface::class)->debug(__CLASS__.__LINE__);
        $log = [
            "request_uri" => $transferObject->getUri(),
            "request" => $result = $this->converter ?
            $this->converter->convert($transferObject->getBody()) :
            $transferObject->getBody()
        ];
        $result = [];
        try {
            $client = $this->clientFactory->create();
            $client->setConfig($transferObject->getClientConfig());
            $client->setMethod($transferObject->getMethod());
            $client->setRawData($transferObject->getBody(), 'application/json');
            $client->setHeaders($transferObject->getHeaders());
            $client->setUrlEncodeBody($transferObject->shouldEncode());
            $client->setUri($transferObject->getUri());
            \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Psr\Log\LoggerInterface::class)->debug(__CLASS__.__LINE__);
            $response = $client->request();
            $result = $this->converter ?
                $this->converter->convert($response->getBody()) :
                [$response->getBody()];
            \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Psr\Log\LoggerInterface::class)->debug(__CLASS__.__LINE__);
            $log["result"] = $result;
        } catch (\Zend_Http_Client_Exception $e) {
            throw new ClientException(__($e->getMessage()));
        } catch (ConverterException $e) {
            throw $e;
        } finally {
            $this->logger->debug($log);
        }

        return $result;
    }
    //put your code here
}
