<?php
namespace Pronko\Authorizenet\Gateway\Converter;

use Magento\Payment\Gateway\Http\ConverterInterface;
use Magento\Framework\Serialize\SerializerInterface;



/**
 * Description of JsonToArray
 *
 * @author marcelo
 */
class JsonToArray implements ConverterInterface
{
    /**     
     * @var SerializerInterface
     */
    private $serializer;
    
    /**
     * 
     * @param SerializerInterface $serializer
     */
    public function __construct(
        SerializerInterface $serializer
    )
    {
        $this->serializer = $serializer;
        
    }

    /**
     * @inheritdoc
     */
    public function convert($response)
    {               
        $response = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response);
        return $this->serializer->unserialize($response);
    }
}
